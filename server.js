const express = require('express'),
bodyParser = require('body-parser'),
Sequelize = require('sequelize')
      
const sequelize = new Sequelize('tw_database','root','',{
  dialect : 'mysql',
  define : {
    timestamps : false
  }
})

const Founder = sequelize.define('founder', {
  name : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,50]
    }
  },
  email : {
    type: Sequelize.STRING,
    allowNull : false,
    validate : {
      isEmail : true
    }
  }
}, {
  underscored : true
})

const Organization = sequelize.define('organization', {
  name : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,100]
    }
  }
}, {
  underscored : true
})

const CryptoCoin = sequelize.define('crypto_coin', {
  name : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [1,100]
    }
  },
  value : {
    type : Sequelize.INTEGER,
    allowNull : true,
    validate : {
      len : [1,100]
    }
  }
}, {
  underscored : true
})

Founder.hasMany(Organization)
Organization.hasMany(CryptoCoin)

const App = express()
App.use(bodyParser.json())

let allPagesCss = `
   html, body{
      margin:0px;
      margin-top: -11px !important;
      
      padding:0px;
  }
  
  .header{
      background-color: #000;
      height: 60px;
      width: 100%;
      text-align: center
  }
  
  .title{
    color: #fff;
    font-family: Arial;
    padding-top: 10px;
  }
  
  ul li{
    list-style-type: none;
  }
  
  ul li a{
    text-decoration: none;
    color: #000;
  }
  
  .content ul li{
    border: 1px solid;
    margin: 5px;
    display: inline-block;
  }
`

let allPagesTemplate = `
<div class="header">
  <div class="top-bar">
      <h1 class="title">Founders And Organizations</h1>
      
  </div>
</div>
`
let main_server_link = 'https://proiect-tehnologii-web-mbleonard.c9users.io/'
let coinflux_api = 'https://api.coinflux.com/api/px/'
let coinflux_market = coinflux_api + 'market'

let bitcoin = coinflux_api + 'XBTEUR'
App.get('/', (req, res, next) => {
  sequelize.sync({force : false})
    let template = `
        <!DOCTYPE html>
          <html>
              <head>
                  <style type="text/css">
                     ${allPagesCss}
                     
                  </style>
              </head>
              <body>
                ${allPagesTemplate}
                <div class="content">
                  <ul>
                      <li><a href="${main_server_link}founders">Show Founders</a></li>
                      <li><a href="${main_server_link}organizations">Show Organizations</a></li>
                      <li><a href="${main_server_link}coins">Show Organization Crypto Raports</a></li>
                      <li><a href="https://api.coinflux.com/api/px/market">Show Crypto Raports</a></li>
      
                  </ul>
                </div>
              </body>
          </html>
    `
    res.send(template)
    .catch((error) => next(error))
})

App.get('/coins', (req, res, next) => {
  CryptoCoin.findAll()
    .then((crypto_coins) =>{
      let template = `
        <!DOCTYPE html>
          <html>
              <head>
                  <style type="text/css">
                     ${allPagesCss}
                     
                  </style>
              </head>
              <body>
                ${allPagesTemplate}
                <div class="content">
                  <ul> 
                      <li><a href="#">${crypto_coins[0].name}</a></li>
                      
      
                  </ul>
                </div>
              </body>
          </html>
    `
    res.send(template)
    })
    .catch((err) => next(err))
})

App.get('/api', (req, res, next) => {
      let btc = JSON.parse(bitcoin)
      let template = `
        <!DOCTYPE html>
          <html>
              <head>
                  <style type="text/css">
                     ${allPagesCss}
                     
                  </style>
              </head>
              <body>
                ${allPagesTemplate}
                <div class="content">
                  <ul> 
                      <li><a href="#">${btc}</a></li>
                      
      
                  </ul>
                </div>
              </body>
          </html>
    `
    res.send(template)
})
App.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((err) => next(err))
})

App.get('/founders', (req, res, next) => {
  Founder.findAll()
    .then((founders) =>{
      let template = `
        <!DOCTYPE html>
          <html>
              <head>
                  <style type="text/css">
                     ${allPagesCss}
                     
                  </style>
              </head>
              <body>
                ${allPagesTemplate}
                <div class="content">
                  <ul> 
                      <li><a href="#">${founders[0].name}</a></li>
                      
      
                  </ul>
                </div>
              </body>
          </html>
    `
    res.send(template)
    })
    .catch((err) => next(err))
})

App.get('/organizations', (req, res, next) => {
  Organization.findAll()
    .then((organization) =>{
      let template = `
        <!DOCTYPE html>
          <html>
              <head>
                  <style type="text/css">
                     ${allPagesCss}
                     
                  </style>
              </head>
              <body>
                ${allPagesTemplate}
                <div class="content">
                  <ul> 
                      <li><a href="#">${organization[0].name}</a></li>
                      
      
                  </ul>
                </div>
              </body>
          </html>
    `
    res.send(template)
    })
    .catch((err) => next(err))
})


App.post('/', (req, res, next) => {
  Founder.create(req.body)
    .then(() => res.status(201).send('First page'))
    .catch((err) => next(err))
})

App.get('/founders/:id', (req, res, next) => {
  Founder.findById(req.params.id)
    .then((founder) => {
      if(founder){
        res.status(200).json(founder)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((err) => next(err))
})

App.get('/organizations/:id', (req, res, next) => {
  Organization.findById(req.params.id)
    .then((organization) => {
      if(organization){
        res.status(200).json(organization)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((err) => next(err))
})


App.put('/founders/:id', (req, res, next) => {
  Founder.findById(req.params.id)
    .then((founder) => {
      if(founder){
        return founder.update(req.body, {fields : ['name', 'email']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))  
})

App.delete('/founders/:id', (req, res, next) => {
    Founder.findById(req.params.id)
    .then((founder) => {
      if(founder){
        return founder.update(req.body, {fields : ['name', 'email']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

App.get('/founders/:fid/organizations', (req, res, next) => {
  Founder.findById(req.params.fid)
    .then((founder) => {
      if (founder){
        return founder.getOrganizations()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((organizations) => {
      if(!res.headers){
        res.status(200).json(organizations)
      }
    })
    .catch((err) => next(err))
})


App.post('/founders/:fid/organizations', (req, res, next) => {
  Founder.findById(req.params.fid)
    .then((founder) => {
      if (founder){
        let organization = req.body
        organization.founder_id = founder.id
        return Organization.create(organization)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((organizations) => {
      if(!res.headers){
        res.status(201).json('created')
      }
    })
    .catch((err) => next(err))
})


App.get('/founders/:fid/organizations/:mid', (req, res, next) => {
  Organization.findById(req.params.mid)
    .then((organization) => {
      if(organization){
        res.status(200).json(organization)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((err) => next(err))
})

App.put('/founders/:fid/organizations/:mid', (req, res, next) => {
  Organization.findById(req.params.mid)
    .then((organization) => {
      if(organization){
        return organization.update(req.body, {fields : ['title', 'content','date']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

App.delete('/founders/:fid/organizations/:mid', (req, res, next) => {
  Organization.findById(req.params.mid)
    .then((organization) => {
      if(organization){
        return organization.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

App.use((err, req, res, next) => {
  console.warn(err)
  res.status(500).send('some error...')
})

App.listen(process.env.PORT)