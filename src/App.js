import React, { Component } from 'react'
import './App.css'
import PersonList from './PersonList'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      persons : [{id: 1, name : 'tim', email : 't@gmail.com'},
      {id: 2, name : 'tom', email : 'tt@gmail.com'}]
    }
    this.addPerson = (person) => {
      let personState = this.state.persons
      personState.push(person)
      this.setState({
        persons : personState
      })
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <PersonList persons={this.state.persons} handleAdd={this.addPerson} />
      </div>
    )
  }
}

export default App
